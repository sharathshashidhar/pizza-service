var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//Get Pizza Order
app.get('/api/pizza/order', function(req, res) {
    console.log("get api called");
    var order_id = req.param('id');

    res.send('OrderId: ' + order_id + ' Status: Your order is being prepared');
});

//Health check
app.get('/api/health', function(req, res) {
    console.log("/api/health called");
    res.send("Health Check Ok");
});

//Post Pizza Order
app.post('/api/pizza/order', function(req, res) {
    console.log("post api called");
    var name = req.body.name;
    var crust = req.body.crust;
    var type = req.body.type;
    var size = req.body.size;
    var extraToppings = req.body.extraToppings;

    if(name!=null && crust!=null && type!=null && size!=null){
        res.send("You order has been successfully placed. \n Order Number: " + Math.floor((Math.random() * 1000) + 1) + '\n Order Time: ' + new Date() + '\n Name: ' + name + '\n Crust: ' + crust + ' \n Type: ' + type + ' \n Size: ' + size + ' \n Extra Toppings: ' + extraToppings);
    }else {
        res.send("Not able to place your order, either 'Name' or 'Crust' or 'Type' or 'Size' is missing");
    }

});

app.get('/', function(req, res) {
    console.log("/ called");
    res.set('Content-Type', 'text/html');
    res.sendFile("index.html", {"root": __dirname});
    //res.send("Welcome to Dominos Pizza \n GET Order: http://localhost:8080/api/pizza/order?id=4 \n POST Order: ");
});

// start the server
app.listen(port);
console.log('Server started! At http://localhost:' + port);
